Issue tracker for "The Endov Society" an online game built with Godot
& Epic Online Services.

Download available here: <https://rancidbacon.itch.io/the-endov-society>

Want Epic Online Services support in *your* Godot game?
Learn how to help an Open Source release of the code in this game happen.
Visit: <https://gitlab.com/RancidBacon/epic-online-services-for-godot>
